package com.orange.persistence.controller;

import com.orange.persistence.model.dto.CreateTransactionResponse;
import com.orange.persistence.model.dto.Report;
import com.orange.persistence.model.dto.TransactionDTO;
import com.orange.persistence.service.ReportService;
import com.orange.persistence.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity addTransaction(@RequestBody TransactionDTO transactionDTO) {

        //method runs async so it returns the response immediatly
        //so i give back an intermediate result
        //by setting the status to STARTED_PROCESSING
        //when the processing is done a message will be published to a kafka que
        //validation microservice subscribes to the topic and the final result of the processing is received
         transactionService.addTransaction(transactionDTO);

         CreateTransactionResponse createTransactionResponse = new CreateTransactionResponse();
         createTransactionResponse.setStatus("STARTED_PROCESSING");
        return  new ResponseEntity<>(createTransactionResponse, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/generateReport/{cnp}", method = RequestMethod.GET)
    public ResponseEntity generateReport(@PathVariable String cnp) {

        Report report = reportService.generateReport(cnp);
        return new ResponseEntity<>(report, HttpStatus.ACCEPTED);
    }

}

package com.orange.persistence.repository;

import com.orange.persistence.model.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

     List<Transaction> getTransactionByCnp(String cnp);
}

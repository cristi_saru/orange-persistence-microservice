package com.orange.persistence.model.dto;

import java.util.List;

public class TransactionReport {

    private TransactionType transactionType;
    private long count;
    private double amount;

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<String> getTransactionsDetails() {
        return transactionsDetails;
    }

    public void setTransactionsDetails(List<String> transactionsDetails) {
        this.transactionsDetails = transactionsDetails;
    }

    private List<String> transactionsDetails;

}

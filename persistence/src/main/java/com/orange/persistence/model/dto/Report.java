package com.orange.persistence.model.dto;

import java.util.List;

public class Report {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public List<TransactionReport> getTransactionReports() {
        return transactionReports;
    }

    public void setTransactionReports(List<TransactionReport> transactionReports) {
        this.transactionReports = transactionReports;
    }

    private String iban;
    private String cnp;

    private List<TransactionReport> transactionReports;

}

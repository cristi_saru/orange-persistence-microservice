package com.orange.persistence.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CreateTransactionResponse {

    @JsonIgnore
    private int transactionId;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }
}

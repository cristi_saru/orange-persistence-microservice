package com.orange.persistence.service;

import com.orange.persistence.model.dto.CreateTransactionResponse;
import com.orange.persistence.model.dto.TransactionDTO;
import com.orange.persistence.model.entity.Transaction;
import com.orange.persistence.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

   // @Autowired
  //  KafkaProducer kafkaProducer;

    @Override
    @Async
    public void addTransaction(TransactionDTO transaction) {
        Transaction savedTransaction = transactionRepository.save(mapToEntity(transaction));
        CreateTransactionResponse response = new CreateTransactionResponse();
        response.setTransactionId(savedTransaction.getId());
        response.setStatus("PROCESSED_SUCCESSFULLY");

//        try {
//            kafkaProducer.sendMessage(response);
//        } catch (IOException ex) {
//
//        }

    }

    private Transaction mapToEntity(TransactionDTO transactionDTO) {
        if (transactionDTO != null) {
            Transaction transaction = new Transaction();
            transaction.setIban(transactionDTO.getSenderIban());
            transaction.setCnp(transactionDTO.getSenderCnp());
            transaction.setTransactionType(transactionDTO.getTransactionType());
            transaction.setName(transactionDTO.getName());
            transaction.setDescription(transactionDTO.getDescription());
            transaction.setAmount(transactionDTO.getAmount());

            return transaction;
        }

        return null;
    }

}

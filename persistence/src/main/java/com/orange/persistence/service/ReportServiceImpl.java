package com.orange.persistence.service;

import com.orange.persistence.model.dto.Report;
import com.orange.persistence.model.dto.TransactionReport;
import com.orange.persistence.model.dto.TransactionType;
import com.orange.persistence.model.entity.Transaction;
import com.orange.persistence.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public Report generateReport(String cnp) {
        List<Transaction> transactions = transactionRepository.getTransactionByCnp(cnp);

        Report report = new Report();
        report.setCnp(cnp);
        report.setIban(transactions.get(0).getIban());
        report.setName(transactions.get(0).getName());

        List<TransactionReport> reports = new ArrayList<>();
        reports.addAll(Arrays.asList(generateReportForTransaction(TransactionType.IBAN_TO_IBAN, transactions)));
        reports.addAll(Arrays.asList(generateReportForTransaction(TransactionType.IBAN_TO_WALLET, transactions)));
        reports.addAll(Arrays.asList(generateReportForTransaction(TransactionType.WALLET_TO_IBAN, transactions)));
        reports.addAll(Arrays.asList(generateReportForTransaction(TransactionType.WALLET_TO_WALLET, transactions)));
        report.setTransactionReports(reports);
        return report;
    }

    private TransactionReport generateReportForTransaction(TransactionType transactionType, List<Transaction> transactions) {
        TransactionReport transactionReport = new TransactionReport();

        List<Transaction> filteredTransactions = transactions.stream().
                filter(transaction -> transaction.getTransactionType() == transactionType)
                .collect(Collectors.toList());

        long totalTransactions = filteredTransactions.stream().count();
        double amount = filteredTransactions.stream().mapToDouble(trans -> trans.getAmount()).sum();
        List<String> transactionDetails = filteredTransactions.stream().map(tr -> tr.getDescription()).collect(Collectors.toList());

        transactionReport.setCount(totalTransactions);
        transactionReport.setAmount(amount);
        transactionReport.setTransactionsDetails(transactionDetails);
        transactionReport.setTransactionType(transactionType);

        return transactionReport;

    }

}

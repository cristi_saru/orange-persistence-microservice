package com.orange.persistence.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.persistence.model.dto.CreateTransactionResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KafkaProducer {

    @Value("${kafka.topic}")
    private String topic;

  //  @Autowired
  //  private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(CreateTransactionResponse createTransactionResponse) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String message = objectMapper.writeValueAsString(createTransactionResponse);

       // this.kafkaTemplate.send(topic, message);
    }
}
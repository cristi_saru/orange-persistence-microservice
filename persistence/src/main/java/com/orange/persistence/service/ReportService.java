package com.orange.persistence.service;

import com.orange.persistence.model.dto.Report;

public interface ReportService {

     Report generateReport(String cnp);
}

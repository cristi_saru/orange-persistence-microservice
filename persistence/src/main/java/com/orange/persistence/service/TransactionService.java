package com.orange.persistence.service;

import com.orange.persistence.model.dto.TransactionDTO;

public interface TransactionService {

    void addTransaction(TransactionDTO transaction);

}
